Oktojam
-------

An accelerometer-controlled music toy made in Löve2D.
Berlin Mini Game Jam October 2014


How to play
-----------

Move your phone to explore the different sounds.
The four colored boxes are mute/unmute buttons.

The fourth instrument is fixed: doesn't change with movement.


Authors
-------
    Desiree Ludewig - Game Concept / Game Design
    Max Heyder - Game Design
    Cédric Douhaire - Sound Design
    Christiaan Janssen - Programming


How to build
------------

You will need Löve2D for Android:
https://bitbucket.org/MartinFelis/love-android-sdl2

Zip the game folder as a Löve file and copy it to the assets directory of the Android project.  Build & Run.

If you try it on the desktop, you can control the sounds with a joystick/gamepad.  But the mute buttons only work with multitouch devices (no mouse input), which means that you will be unable to mute sounds in the desktop.  I leave the implementation of that as an exercise for the reader.


