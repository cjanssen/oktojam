--------------------------------
-- graphs
function initColors()
	colors = {
		{244,243,130},
		{179,218,135},
		{183,163,141},
		{109,104,129},
		{37,51,59},
	}
end

function initGraphs()
	glim = 400
	graph = {{},{},{}}
end


function updateGraphs()
	if love.joystick.getJoystickCount() > 0 then
		-- first joystick is accelerometer
		local joy = love.joystick.getJoysticks()[1]

		local filtercoef = 0.1
		local values = { joy:getAxes() }

		for valndx,val in ipairs(values) do
			local g = graph[valndx]
			local oldval = #g>0 and g[#g] or val
			local newval = val * filtercoef + oldval * (1-filtercoef)
			table.insert(g, newval)
			while #g > glim do table.remove(g,1) end

			-- ignore other axes if they exist
			if valndx >= 3 then break end
		end
	end
end


function drawGraph()
	local ww,hh = love.graphics.getWidth(),love.graphics.getHeight()

	for i=1,3 do
		local line = {}
		for jj = 1,#graph[i] do
			table.insert(line, jj * ww / glim)
			table.insert(line, graph[i][jj] * (hh/2) + (hh/2) )
		end

		if #line > 2 then
			love.graphics.setColor(unpack(colors[i]))
			love.graphics.line(line)
		end
	end
end


-------------------------------
-- buttons
function initUI()
	UIboxes = {}

	for i=0,3 do
		local xx = i
		table.insert(UIboxes,{
			x = xx * 1/4 + 1/16, y = 1/4, w = 1/4 - 2/16, h = 1/4,
			color = colors[i+1], idx = i, pressed = false, waspressed = false, active = true,
			}
		)
	end
end



function updateUI()
	for _,uiBox in ipairs(UIboxes) do
		uiBox.pressed = false
		for _,finger in pairs(fingers) do
			local mx,my = finger.x,finger.y
			if mx >= uiBox.x and mx <= uiBox.x + uiBox.w and my >= uiBox.y and my <= uiBox.y + uiBox.h then
				uiBox.pressed = true				
			end
		end
	end
end

function drawUI()
	local ww,hh = love.graphics.getWidth(),love.graphics.getHeight()
	for _,uiBox in ipairs(UIboxes) do
		local r,g,b = unpack(uiBox.color)
		local br,bg,bb = unpack(colors[5])
		local alpha = 0.15
		if uiBox.active then
			alpha = 0.45
		end
		love.graphics.setColor(r*alpha + br*(1-alpha), g*alpha + bg*(1-alpha), b*alpha + bb*(1-alpha))

		love.graphics.rectangle("fill", uiBox.x *ww, uiBox.y*hh, uiBox.w*ww, uiBox.h*hh)
	end
end


-----------------------------
-- multitouch

function initTouch()
	fingers = {}
end

function love.touchpressed(_b,mx,my,_p)
	fingers[_b] = {x = mx,y = my}
end

function love.touchreleased(_b,mx,my,_p)
	fingers[_b] = nil
end

function love.touchmoved(_b,mx,my,_p)
	love.touchreleased(_b,mx,my,_p)
	love.touchpressed(_b,mx,my,_p)
end


-----------------------------
-- sound
function initSounds()
	notes = {
		-- first instrument
		{
		love.audio.newSource("snd/DM.ogg"),
		love.audio.newSource("snd/Dsus4.ogg"),
		love.audio.newSource("snd/GMD.ogg"),
		love.audio.newSource("snd/Gsus2.ogg"),
		love.audio.newSource("snd/AmC.ogg"),
		},

		-- second instrument
		{
		love.audio.newSource("snd/Dlong.ogg"),
		love.audio.newSource("snd/Elong.ogg"),
		love.audio.newSource("snd/F#long.ogg"),
		love.audio.newSource("snd/Along.ogg"),
		love.audio.newSource("snd/Blong.ogg"),
		},

		-- third instrument
		{	
		love.audio.newSource("snd/ambiance1.ogg"),
		love.audio.newSource("snd/ambiance2.ogg"),
		love.audio.newSource("snd/ambiance3.ogg"),
		love.audio.newSource("snd/ambiance4.ogg"),
		love.audio.newSource("snd/ambiance5.ogg"),
		},
	}

	for _,notegroup in ipairs(notes) do
		for _,note in ipairs(notegroup) do
			note:setVolume(0)
			note:setLooping(true)
			note:play()
		end
	end

	droneSnd = love.audio.newSource("snd/Drone.ogg")
	droneSnd:setLooping(true)
	droneSnd:play()

	currentNotes = {}
end

function updateSound()
	-- if button was pressed, enable/disable corresponding sound
	for uiIndex,uiBox in ipairs(UIboxes) do
		if uiBox.waspressed ~= uiBox.pressed then
			uiBox.waspressed = uiBox.pressed
			if uiBox.pressed then
				uiBox.active = not uiBox.active

				if uiIndex < 4 and currentNotes[uiIndex] then
					if uiBox.active then
						if uiIndex == 2 then
							currentNotes[uiIndex]:setVolume(0.5)
						else
							currentNotes[uiIndex]:setVolume(1)
						end
					else
						currentNotes[uiIndex]:setVolume(0)
					end
				elseif uiIndex == 4 then
					if uiBox.active then
						droneSnd:setVolume(1)
					else
						droneSnd:setVolume(0)
					end
				end			
			end
		end
	end

	-- select note based on graph
	for soundNdx,g in ipairs(graph) do
		if #g > 0 then
			local val = g[#g]
			local angleNdx = math.max(1, math.min(5, math.ceil((val + 1)/2 * 5)))

			if currentNotes[soundNdx] ~= notes[soundNdx][angleNdx] then
				if currentNotes[soundNdx] then 
					currentNotes[soundNdx]:setVolume(0)
				end
				currentNotes[soundNdx] = notes[soundNdx][angleNdx]
				if currentNotes[soundNdx] and UIboxes[soundNdx].active then
					currentNotes[soundNdx]:setVolume(1)
					if soundNdx==2 then currentNotes[soundNdx]:setVolume(0.3) end
				end
			end
		end
	end
end


---------------------------
-- love2d callbacks
function love.load()
	initColors()
	initGraphs()
	initTouch()
	initUI()
	initSounds()
end

function love.update(dt)
	updateUI()
	updateGraphs()
	updateSound()
end

function love.draw()
	love.graphics.setBackgroundColor(unpack(colors[5]))
	drawUI()
	drawGraph()
end